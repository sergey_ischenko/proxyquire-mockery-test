Here are a few experiments on complete/partial stubbing CommonJS modules (what is useful in tests)
with two alternative instruments: [proxiquire](https://www.npmjs.com/package/proxyquire) and [mockery](https://www.npmjs.com/package/mockery).

If you don't see what it's for at all - read [this](http://howtonode.org/testing-private-state-and-mocking-deps) please (the introductory part there, as for me, is a very clear explaination of the problem).

## Conclusions:

* **Both proxiquire and mockery** are good enough - at least they both **work**.

* **Mockery Pros:** You stub a module apart from requiring it and always use a regular `require` function to require it (particularly this allows to stub directly a required module - see "Proxyquire Cons"):

```sh
mockery.registerMock('fs', {
  readFileSync: function() { return 'Fake content'; }
});
var baz = require('../src/baz');
```


* **Mockery Cons:** It needs more configuration (enable/disable, registerAllowables/deregisterAllowables, useCleanCache...).
If you forget that, you will encounter some module not loaded/reloaded. See check/mockery_check.js.

* **Proxyquire Pros:**
  ** You can have many differently stubbed versions of the same module in the same context (in the same test suite), not being forced to re-register the module.

* **Proxyquire Cons:**

    * With proxyquire, in contrast to mockery, **it's IMPOSSIBLE to stub the export of a directly required module**, it affects dependent modules only. See check/proxyquire_check.js.
It's not a big problem in a test environment (mocking libraries provide an instrument for stubbing on the first level, that is stubbing a direct under-test module), however it's a fact.

    * You should use a special `proxyquire` function instead of a reqular `require`, you stub a module not directly, but via the proxyquire parameter:

```sh
var baz = proxyquire('../src/baz', {
  'fs': { readFileSync: function() { return 'Fake content'; }}
});
```


## The resume:
**proxiquire is simpler in use than mockery**, however mockery looks like more powerful.


To see the experiments in action clone the repository and do the following:

    npm install
    npm run check
    npm run spec
