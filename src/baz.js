var fs = require('fs');

module.exports = {

  method: function() {
    return 'I am the original Baz';
  },

  readMyFile: function(fileName) {
    return fs.readFileSync(fileName, 'utf8');
  }
}
