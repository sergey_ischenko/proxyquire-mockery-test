// The complete require tree:
// ./src/foo => function
//   bar => function                  <--- entire bar's export will be stubbed in one example
//     baz => { method: function }    <--- method implementation will be stubbed in another example

require('./check/proxyquire_check');

require('./check/mockery_check');

console.info('\n*** Also see README.md.');



