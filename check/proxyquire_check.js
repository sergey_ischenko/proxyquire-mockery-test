var proxyquire = require('proxyquire');

require('../helpers').info('Cheking proxyquire');

var foo = require('../src/foo');
foo(); // 'I am the original Baz' is printed to stdout

var fooWithMockedBaz = proxyquire('../src/foo', {
  './baz': {
    method: function(val) { return "I am a fake Baz's method" },
    '@global': true
  }
});
fooWithMockedBaz();  // "I am a fake Baz's method" is printed to stdout

var fooWithMockedBar = proxyquire('../src/foo', {
  './bar': function(){ return "I am a competely fake Bar's export"; }
});
fooWithMockedBar();  // "I am a competely fake Bar's export" is printed to stdout

console.info("\nStubbing directly (right from this script) required 'fs' module:");
console.info("With proxyquire it's IMPOSSIBLE to stub the export of a directly required module, it affects `require` in dependent modules only.");
// var readTheFile = function(fileName) {
//   var fs = proxyquire('fs', {
//     readFileSync: function(val) { return "Hello from a README stub!"; },
//     '@global': true
//   });
//   return fs.readFileSync(fileName, 'utf8');
// }
// console.info('Fake README: ' + readTheFile('README.md'));
