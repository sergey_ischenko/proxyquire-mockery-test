var mockery = require('mockery');

require('../helpers').info('Cheking mockery');

var originalFoo = require('../src/foo');
originalFoo(); // 'I am the original Baz' is printed to stdout

mockery.enable({
  useCleanCache: true // This is needed to forget previously loaded '../src/foo' module.
});


var registerAllowables = function(){
  mockery.registerAllowables(['../src/foo', './bar', './baz', 'fs'], true);
};

registerAllowables();
mockery.registerMock('./baz', {
  method: function(val) { return "I am a fake Baz's method"; }
});
fooWithMockedBaz = require('../src/foo');
fooWithMockedBaz();  // "I am a fake Baz's method" is printed to stdout
mockery.deregisterAll();

registerAllowables();
mockery.registerMock('./bar', function(){ return "I am a competely fake Bar's export"; });
fooWithMockedBar = require('../src/foo');
fooWithMockedBar();  // "I am a competely fake Bar's export" is printed to stdout
mockery.deregisterAll();

console.info("\nStubbing directly (right from this script) required 'fs' module:");
mockery.registerAllowables(['../src/baz', 'fs'], true);
mockery.registerMock('fs', {
  readFileSync: function(val) { return "Hello from a README stub!"; }
});
var readTheFile = function(fileName) {
  var fs = require('fs');
  return fs.readFileSync(fileName, 'utf8');
};
console.info('Fake README: ' + readTheFile('README.md'));
mockery.deregisterAll();

mockery.disable();
