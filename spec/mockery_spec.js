var mockery = require('mockery');

describe('Use of mockery', function(){

  beforeAll(function(){
    require('../helpers').info('Testing mockery');
  });

  beforeEach(function(){
    mockery.enable();
  });

  afterEach(function(){
    mockery.deregisterAll();
    mockery.disable();
  });

  describe('Foo', function(){
    beforeEach(function(){
      mockery.registerAllowables(['../src/foo', './bar', './baz', 'fs'], true);
    });

    describe('using the original Baz', function(){
      var foo;

      beforeEach(function() {
        foo = require('../src/foo');
      });

      it("prints 'I am the original Baz'", function(){
        expect(foo()).toEqual('I am the original Baz');
      });
    });

    describe('using a stubbed method of Baz', function(){
      var foo;
      var double = {
        method: function(val) { return "I am a fake Baz's method"; }
      };

      beforeEach(function() {
        mockery.registerMock('./baz', double);
        foo = require('../src/foo');
      });

      it("returns the value taken from the double", function(){
        expect(foo()).toEqual("I am a fake Baz's method");
      });

      it("can be spied properly", function(){
        spyOn(double, 'method').and.returnValue("Output of 'method' Spy (to not confuse you with undefined)");
        foo();
        expect(double.method).toHaveBeenCalled();
      });
    });

    describe('using a completely stubbed Bar', function(){
      var foo;
      var double;

      beforeEach(function() {
        double = jasmine.createSpy('Fake-bar-export').and.returnValue("Output of 'Fake-bar-export' Spy (to not confuse you with undefined)");
        mockery.registerMock('./bar', double);
        foo = require('../src/foo');
      });

      it("returns the value taken from the double", function(){
        double.and.callFake(function(){ return "I am a competely fake Bar's export"; });
        expect(foo()).toEqual("I am a competely fake Bar's export");
      });

      it("can be spied properly", function(){
        foo();
        expect(double).toHaveBeenCalled();
      });
    });

  });

  describe("Something closer to real life (stubbing 'fs')", function(){
    var baz;
    var result;

    beforeEach(function() {
      mockery.registerAllowables(['../src/baz', 'fs'], true);
    });

    describe("baz#readMyFile using the original 'fs'", function(){
      beforeEach(function() {
        baz = require('../src/baz');
        result = baz.readMyFile('./README.md');
      });

      it("reads a real file", function(){
        expect(baz.readMyFile('./README.md')).toMatch(/stubbing CommonJS modules/);
      });
    });

    describe("baz#readMyFile using a stubbed 'fs'", function(){
      beforeEach(function() {
        mockery.registerMock('fs', {
          readFileSync: function() { return 'Fake content'; }
        });
        baz = require('../src/baz');
        result = baz.readMyFile('./README.md');
      });

      it("does not read a real file", function(){
        expect(result).not.toMatch(/stubbing CommonJS modules/);
      });

      it("returns a fake value", function(){
        expect(result).toEqual('Fake content');
      });

      it("does not affect a not touched method", function(){
        expect(baz.method()).toEqual('I am the original Baz');
      });
    });
  });

});
