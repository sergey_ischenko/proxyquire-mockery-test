beforeAll(function(){
  var SpecReporter = require('jasmine-spec-reporter');
  jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));
});
