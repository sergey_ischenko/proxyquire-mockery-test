var path = require('path');
var stackTrace = require('stack-trace');


module.exports = {
  info: function(msg, req) {
    var caller = path.basename(stackTrace.get()[1].getFileName());
    console.info('\n>>>> ' + msg + ' (see ' + path.relative(process.cwd(), caller) +')...\n');
  }
};
